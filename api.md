# Wordpress
* 在Wordpress中增加插件json-api，可以通过api方式调用获取内容，内容以json格式返回。  
* DataHub的Wordpress的文章分类如下：  
[分类名] [id]  
未分类  1   
动态 2  
观点 3  
活动 8  
最佳实践 9  
专栏 55  

* 主要接口：  
获取文章分类：http://blog.hub.dataos.io/?json=get_category_index  
获取最新的文章：http://blog.hub.dataos.io/?json=get_recent_posts  
按文章类别获取：http://blog.hub.dataos.io/?json=get_category_posts&category_id=8&count=3  

* 说明：
(1)count为获取条数
(2)返回结果中主要用以下字段：  
	title 文章标题  
	date 文章日期

* 返回示例：  

	{
	 "status": "ok",
	 "count": 3,
	 "pages": 1,
	 "category": {
	 "id": 8,
	 "slug": "activities",
	 "title": "活动",
	 "description": "",
	 "parent": 0,
	 "post_count": 3
	  },
	  "posts": [
	    {
	      "id": 1093,
	      "type": "post",
	      "slug": "datahub%e6%b4%bb%e5%8a%a8-%e4%ba%9a%e4%bf%a1%e6%95%b0%e6%8d%ae%e7%89%b5%e6%89%8baws%e5%85%b1%e7%ad%91%e4%ba%91%e7%ab%af%e5%a4%a7%e6%95%b0%e6%8d%ae%e7%94%9f%e6%80%81",
	      "url": "http://blog.hub.dataos.io/archives/1093",
	      "status": "publish",
	      "title": "DataHub活动 |亚信数据牵手AWS共筑云端大数据生态",
	      "title_plain": "DataHub活动 |亚信数据牵手AWS共筑云端大数据生态",
	      "content": "作者：何鸿凌 亚信科技是伴随着中国互联网成长的步伐建立起来的。1993年，田朔宁、丁健等小伙伴创办亚信科技，希望构筑中国互联网的“基础设施”，在ChinaNet的建设中贡献了重要的力量；11年后，也就是2002年，亚信科技以融资为契机进行转型，全面转向企业级应用软件提供商，包括CRM、Billin",
	      "date": "2016-09-12 13:57:06",
	      "modified": "2016-09-12 13:57:06",
	      "categories": [
	        {
	          "id": 8,
	          "slug": "activities",
	          "title": "活动",
	          "description": "",
	          "parent": 0,
	          "post_count": 3
	        }
	      ],
	      "tags": [],
	      "author": {
	        "id": 1,
	        "slug": "admin",
	        "name": "admin",
	        "first_name": "",
	        "last_name": "",
	        "nickname": "admin",
	        "url": "",
	        "description": ""
	      },
	      "comments": [],
	      "attachments": [
	        {
	          "id": 1094,
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1.jpg",
	          "slug": "1-5",
	          "title": "1",
	          "description": "",
	          "caption": "",
	          "parent": 1093,
	          "mime_type": "image/jpeg",
	          "images": {
	            "full": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1.jpg",
	              "width": 900,
	              "height": 500
	            },
	            "thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1-150x150.jpg",
	              "width": 150,
	              "height": 150
	            },
	            "medium": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1-300x167.jpg",
	              "width": 300,
	              "height": 167
	            },
	            "post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1-100x100.jpg",
	              "width": 100,
	              "height": 100
	            },
	            "single-post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1-170x170.jpg",
	              "width": 170,
	              "height": 170
	            },
	            "portfolio-item-small": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/1-300x250.jpg",
	              "width": 300,
	              "height": 250
	            }
	          }
	        },
	        {
	          "id": 1095,
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2.jpg",
	          "slug": "2-6",
	          "title": "2",
	          "description": "",
	          "caption": "",
	          "parent": 1093,
	          "mime_type": "image/jpeg",
	          "images": {
	            "full": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2.jpg",
	              "width": 900,
	              "height": 500
	            },
	            "thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2-150x150.jpg",
	              "width": 150,
	              "height": 150
	            },
	            "medium": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2-300x167.jpg",
	              "width": 300,
	              "height": 167
	            },
	            "post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2-100x100.jpg",
	              "width": 100,
	              "height": 100
	            },
	            "single-post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2-170x170.jpg",
	              "width": 170,
	              "height": 170
	            },
	            "portfolio-item-small": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/09/2-300x250.jpg",
	              "width": 300,
	              "height": 250
	            }
	          }
	        }
	      ],
	      "comment_count": 0,
	      "comment_status": "open",
	      "custom_fields": {}
	    },
	    {
	      "id": 622,
	      "type": "post",
	      "slug": "%e5%aa%92%e4%bd%93%e6%8a%a5%e9%81%93-%e6%95%b0%e6%8d%ae%e6%b5%81%e9%80%9a%e5%b9%b3%e5%8f%b0datahub%e4%ba%ae%e7%9b%b8%e6%95%b0%e5%8d%9a%e4%bc%9a%ef%bc%8c%e5%85%b1%e9%93%b8%e6%95%b0%e6%8d%ae%e4%ba%92",
	      "url": "http://blog.hub.dataos.io/archives/622",
	      "status": "publish",
	      "title": "媒体报道 | 数据流通平台DataHub亮相数博会，共铸数据互联网",
	      "title_plain": "媒体报道 | 数据流通平台DataHub亮相数博会，共铸数据互联网",
	      "content": "转自：财经中国网 2016数博会在贵阳顺利召开，亚信数据大数据云平台部总经理何鸿凌受邀参加“块数据城市”——大数据产业创新发展论坛，并发表了“共铸数据互联网”主题演讲。在与媒体交流时，何鸿凌表示，“近几年大数据应用落地成为关注的焦点，而数据流通则成为大数据应用可以落地的关键。DataHub致",
	      "date": "2016-05-27 13:41:58",
	      "modified": "2016-05-27 13:41:58",
	      "categories": [
	        {
	          "id": 8,
	          "slug": "activities",
	          "title": "活动",
	          "description": "",
	          "parent": 0,
	          "post_count": 3
	        }
	      ],
	      "tags": [
	        {
	          "id": 56,
	          "slug": "datahub",
	          "title": "DataHub",
	          "description": "",
	          "post_count": 13
	        },
	        {
	          "id": 14,
	          "slug": "%e5%a4%a7%e6%95%b0%e6%8d%ae",
	          "title": "大数据",
	          "description": "",
	          "post_count": 76
	        },
	        {
	          "id": 62,
	          "slug": "%e6%95%b0%e6%8d%ae%e4%ba%a4%e6%98%93",
	          "title": "数据交易",
	          "description": "",
	          "post_count": 8
	        },
	        {
	          "id": 35,
	          "slug": "%e6%95%b0%e6%8d%ae%e6%b5%81%e9%80%9a",
	          "title": "数据流通",
	          "description": "",
	          "post_count": 27
	        }
	      ],
	      "author": {
	        "id": 1,
	        "slug": "admin",
	        "name": "admin",
	        "first_name": "",
	        "last_name": "",
	        "nickname": "admin",
	        "url": "",
	        "description": ""
	      },
	      "comments": [],
	      "attachments": [
	        {
	          "id": 623,
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图.jpg",
	          "slug": "0526%e5%9b%be",
	          "title": "0526图",
	          "description": "",
	          "caption": "",
	          "parent": 622,
	          "mime_type": "image/jpeg",
	          "images": {
	            "full": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图.jpg",
	              "width": 1906,
	              "height": 1200
	            },
	            "thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图-150x150.jpg",
	              "width": 150,
	              "height": 150
	            },
	            "medium": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图-300x189.jpg",
	              "width": 300,
	              "height": 189
	            },
	            "post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图-100x100.jpg",
	              "width": 100,
	              "height": 100
	            },
	            "single-post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图-170x170.jpg",
	              "width": 170,
	              "height": 170
	            },
	            "portfolio-item-small": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图-300x250.jpg",
	              "width": 300,
	              "height": 250
	            }
	          }
	        },
	        {
	          "id": 625,
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2.jpg",
	          "slug": "0526%e5%9b%be2",
	          "title": "0526图2",
	          "description": "",
	          "caption": "",
	          "parent": 622,
	          "mime_type": "image/jpeg",
	          "images": {
	            "full": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2.jpg",
	              "width": 1060,
	              "height": 463
	            },
	            "thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2-150x150.jpg",
	              "width": 150,
	              "height": 150
	            },
	            "medium": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2-300x131.jpg",
	              "width": 300,
	              "height": 131
	            },
	            "post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2-100x100.jpg",
	              "width": 100,
	              "height": 100
	            },
	            "single-post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2-170x170.jpg",
	              "width": 170,
	              "height": 170
	            },
	            "portfolio-item-small": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/05/0526图2-300x250.jpg",
	              "width": 300,
	              "height": 250
	            }
	          }
	        }
	      ],
	      "comment_count": 0,
	      "comment_status": "open",
	      "custom_fields": {}
	    },
	    {
	      "id": 278,
	      "type": "post",
	      "slug": "%e5%88%86%e4%ba%ab-%e6%89%93%e9%80%9a%e6%95%b0%e6%8d%ae%e5%a3%81%e5%9e%92%ef%bc%8c%e9%87%8a%e6%94%be%e6%94%bf%e5%ba%9c%e5%a4%a7%e6%95%b0%e6%8d%ae%e4%bb%b7%e5%80%bc",
	      "url": "http://blog.hub.dataos.io/archives/278",
	      "status": "publish",
	      "title": "分享 | 打通数据壁垒，释放政府大数据价值",
	      "title_plain": "分享 | 打通数据壁垒，释放政府大数据价值",
	      "content": "亚信数据大数据云平台部 郭媛媛 近日，由2016年数博会组委会主办，中国大数据产业观察网承办，中关村大数据产业联盟协办的“大数据在城市治理中的应用和痛点”沙龙如期在中关村创业大街黑马会召开。",
	      "excerpt": "亚信数据大数据云平台部",
	      "date": "2016-03-18 14:29:50",
	      "modified": "2016-03-18 14:33:34",
	      "categories": [
	        {
	          "id": 8,
	          "slug": "activities",
	          "title": "活动",
	          "description": "",
	          "parent": 0,
	          "post_count": 3
	        }
	      ],
	      "tags": [
	        {
	          "id": 32,
	          "slug": "%e4%ba%a4%e6%8d%a2%e6%a0%87%e5%87%86",
	          "title": "交换标准",
	          "description": "",
	          "post_count": 2
	        },
	        {
	          "id": 35,
	          "slug": "%e6%95%b0%e6%8d%ae%e6%b5%81%e9%80%9a",
	          "title": "数据流通",
	          "description": "",
	          "post_count": 27
	        }
	      ],
	      "author": {
	        "id": 1,
	        "slug": "admin",
	        "name": "admin",
	        "first_name": "",
	        "last_name": "",
	        "nickname": "admin",
	        "url": "",
	        "description": ""
	      },
	      "comments": [],
	      "attachments": [
	        {
	          "id": 280,
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1.jpg",
	          "slug": "1",
	          "title": "1",
	          "description": "",
	          "caption": "",
	          "parent": 278,
	          "mime_type": "image/jpeg",
	          "images": {
	            "full": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1.jpg",
	              "width": 1280,
	              "height": 484
	            },
	            "thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1-150x150.jpg",
	              "width": 150,
	              "height": 150
	            },
	            "medium": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1-300x113.jpg",
	              "width": 300,
	              "height": 113
	            },
	            "post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1-100x100.jpg",
	              "width": 100,
	              "height": 100
	            },
	            "single-post-thumbnail": {
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1-170x170.jpg",
	              "width": 170,
	              "height": 170 
	            }, 
	            "portfolio-item-small": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/1-300x250.jpg", 
	              "width": 300, 
	              "height": 250 
	            } 
	          } 
	        }, 
	        { 
	          "id": 281, 
	          "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2.jpg", 
	          "slug": "2", 
	          "title": "2", 
	          "description": "", 
	          "caption": "", 
	          "parent": 278, 
	          "mime_type": "image/jpeg", 
	          "images": { 
	            "full": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2.jpg", 
	              "width": 936, 
	              "height": 702 
	            }, 
	            "thumbnail": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2-150x150.jpg", 
	              "width": 150, 
	              "height": 150 
	            }, 
	            "medium": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2-300x225.jpg", 
	              "width": 300, 
	              "height": 225 
	            }, 
	            "post-thumbnail": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2-100x100.jpg", 
	              "width": 100, 
	              "height": 100 
	            }, 
	            "single-post-thumbnail": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2-170x170.jpg", 
	              "width": 170, 
	              "height": 170 
	            }, 
	            "portfolio-item-small": { 
	              "url": "http://blog.hub.dataos.io/wp-content/uploads/2016/03/2-300x250.jpg", 
	              "width": 300, 
	              "height": 250 
	            } 
	          } 
	        } 
	      ], 
	      "comment_count": 0, 
	      "comment_status": "open", 
	      "custom_fields": {} 
	    } 
	  ] 
	} 
